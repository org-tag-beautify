* Intro

#+ATTR_ORG: :width 600
#+ATTR_LATEX: :width 6.0in
#+ATTR_HTML: :width 600px
[[file:screenshot.png]]

* Features

- display lots of tags as icons
- support unicode emojis as tag icons
- auto add Org tags based on ~org-attach~ command attached file types.

* Installation

This package is available on MELPA already!

* Changelog

- Now this package switched icon support from package all-the-icons to nerd-icons.

* Usage

#+begin_src emacs-lisp
(use-package org-tag-beautify
  :ensure t
  :custom (org-tag-beautify-data-dir "~/Code/Emacs/org-tag-beautify/data/")
  :init (org-tag-beautify-mode 1))
#+end_src

